# Simply require all files on lib directory
%w(lib).each do |dir|
  Dir[File.join(__dir__, dir, '*.rb')].each {|file| require file}
end

def main

  app = App.new

  filters = Util.get_filter_args
  group_filters = Util.get_args Util::FLAGS::GROUPINGS

  cards = app.get_cards filters, group_filters

  App.print_cards cards
end

main