%w(lib).each do |dir|
  Dir[File.join(__dir__, dir, '*.rb')].each {|file| require file}
end

class Test

  def test_group_filters
    MTG_API.set_max_pages Curtain::DEFAULT_CHILDREN

    test_duplicated_group
    test_non_existent_group
    test_set_group
    test_rarity_group
  end

  def test_non_existent_group
    app = App.new
    app.get_cards({}, ['set', 'rarity'])
    expected_count = app.cards.count

    app = App.new
    app.get_cards({}, ['set', 'rarity', 'phantom'])
    result = app.cards.count

    assert expected_count, result, true
  end

  def test_duplicated_group
    app = App.new
    app.get_cards({}, ['set', 'rarity'])
    expected_count = app.cards.count

    app = App.new
    app.get_cards({}, ['set', 'rarity', 'set'])
    result = app.cards.count

    assert expected_count, result, true
  end

  def test_set_group
    cards = App.new.get_cards({}, ['set'])
    # validate that first item is a hash grouping the array
    assert true, cards.is_a?(Array)
    assert true, cards.count >= 1
    assert true, cards.first.is_a?(Hash)
    assert true, cards.first.first.is_a?(Array), true
  end

  def test_rarity_group
    cards = App.new.get_cards({}, ['set', 'rarity'])
    # validate that first item is a hash grouping the array
    assert true, cards.is_a?(Array)
    assert true, cards.count >= 1
    assert true, cards.first.is_a?(Hash)
    assert true, cards.first.first.is_a?(Array), true

    group = cards.first.first

    # check first is name of group and last is array of hashes(cards)
    assert true, group.first.is_a?(String)
    assert true, group.last.is_a?(Array)
    assert true, group.last.first.is_a?(Hash), true
  end

  def test_ktk_filter
    MTG_API.set_max_pages 400

    # validate both return same count of cards
    app = App.new
    app.get_cards({'colors' => ['Red', 'Blue'], 'set' => ['KTK']})
    cards = app.cards
    response = JSON.parse(Util::NET.get('https://api.magicthegathering.io/v1/cards', set: 'KTK', colors: 'Red,Blue').body)['cards']
    assert response.count, cards.count, true
  end

  def test
    t = Time.now
    puts "Test Started at #{t}"

    test_group_filters
    test_ktk_filter

    puts "Test Finished after #{(Time.now - t)} seconds"
  end


  def assert(expected, result, print_passed = false)
    passed = expected == result
    if passed
      puts "Assertion passed expected: #{expected} and got: #{result}" if print_passed
    else
      puts "Assertion failed expected: #{expected} but got: #{result} in #{Thread.current.backtrace.take(4).join("\n")}"
    end
  end
end

Test.new.test

