require 'etc'
require 'tempfile'
require 'fileutils'

# Curtain is a simple concern class to process our Thread/Process handling
# this class can easily be scalated to use +Process+ instead of Thread to leverage
# a computers multiple Cores instead of just using 1
class Curtain
  DEFAULT_CHILDREN = 9

  def initialize(child)
    @child = child
  end

  def wait
    @child.join
  end

  # By default just return 4 max threads
  def self.max_children
    Curtain::DEFAULT_CHILDREN
  end

  # Spawns our new Thread and yields the block passed,
  # returns a new +Curtain+ object with the process id attached to it.
  def self.drop

    child = Thread.fork do
      yield
      # exit 0
    end

    Curtain.new child
  end

end
