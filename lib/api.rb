require 'json'
require 'concurrent'
require 'zlib'
require 'base64'

# Base Interface for out API implementation
class API

  def get_cards(filters = {})
    raise StandardError("Not Implemented Error: #{self.class.name} must implement get_cards(filters={})")
  end

end

class MTG_API < API

  API_URI = 'https://api.magicthegathering.io/v1/cards'
  @max_pages = 400

  attr_accessor :total_cards
  attr_accessor :cards_per_page
  attr_accessor :remaining_rate_limit

  def self.max_pages
    @max_pages
  end

  def self.set_max_pages(pages)
    @max_pages = pages
  end

  # get required info by making an initial request to the API
  def initialize
    self.get_info
  end

  def get_info
    headers = Util::NET.get(MTG_API::API_URI).header

    self.total_cards = headers['Total-Count'].to_i
    self.cards_per_page = headers['Count'].to_i
    self.remaining_rate_limit = headers['Ratelimit-Remaining'].to_i

    headers
  end

  # To filter cards check that all the values inside our filter hash,
  # matches any expecific card
  def filter_cards(cards, filters)
    return cards if !filters || filters.empty?
    return cards unless cards

    result = []
    result += cards.select do |c|
      included = true

      # Only "select" the cards that matches all of our filters
      filters.each do |filter, values|
        card_attr = c[filter]
        included &= card_attr && values.all? {|value| card_attr == value || (card_attr.include?(value) if card_attr.is_a?(Array))}
      end

      included
    end

    result.uniq
  end

  # Retrieve an specific page from the API
  def get_cards_page(page, filters = {})
    res = Util::NET.get MTG_API::API_URI, page: page
    res = JSON.parse(res.body)['cards']

    # filter the result before returning
    filter_cards(res, filters)
  end

  # calculate the amount of pages per thread we should process based on the
  # total available cards on the API
  def get_pages_per_process(thread_count)
    pages = self.total_cards / self.cards_per_page
    # For debugging purposes allow class variable to be set to enable testing
    pages = pages > MTG_API.max_pages ? MTG_API.max_pages : pages

    # If we won't be allowed to continue because of rate limit, raise an error
    if self.remaining_rate_limit < pages
      raise StandardError.new('Not enough Quota Left to complete the card scraping. Try again in 1 hour.')
    end

    pages / thread_count
  end

  def get_cards_with_threads(filters = {})
    # Use a Thread-safe Array to enable our threads to write to it
    cards = Concurrent::Array.new

    # Get the amount of threads we need and the pages each thread will handle
    thread_count = Curtain.max_children
    pages_per_process = get_pages_per_process(thread_count)

    threads = []

    # spawn our threads the required amount of times and process each page
    thread_count.times do |thread_num|
      threads << Curtain.drop do

        start_page = thread_num * pages_per_process
        end_page = pages_per_process + start_page
        pages = start_page...end_page

        # add the result to our cards array when done
        pages.each do |page|
          cards += get_cards_page page, filters
        end
      end
    end

    # Wait on all threads to finish
    threads.each {|t| t.wait}

    # Make sure to not get repeated cards
    cards.uniq!

    cards
  end

  def get_cards(filters = {})
    cards = get_cards_with_threads filters

    # yield the result if a block was passes or simply return the cards
    if block_given?
      yield cards
    else
      cards
    end
  end
end

# Just a Mock API used for TDD when creating basic arch for the app
class Mock_API < API

  def get_cards(filters = {})

    cards = []

    number.times {cards << random_card}

    cards
  end

  def number
    (Random.new.rand * 100 / 10).ceil
  end

  def random_card
    Random.new.rand
    {
        name: "Card #{number}",
        set: "Set #{number}"
    }
  end

end