class App

  attr_accessor :api
  attr_accessor :cards
  attr_accessor :grouped_cards

  def initialize
    self.cards = []
    self.grouped_cards = []

    # initialize our API
    self.api = MTG_API.new
  end

  def get_cards(filters = {}, group_filters = [])
    # Ensure default values in case nil is passed
    group_filters = group_filters || []
    group_filters = group_filters.uniq || []
    filters = filters || {}

    self.cards = self.api.get_cards filters
    self.grouped_cards = self.group_cards self.cards, group_filters
  end

  def group_cards(cards, group_filters)
    return cards unless group_filters
    return cards unless group_filters.any?

    # Make sure to only filter by attributes that out cards actually have
    group_filters = clear_group_filters group_filters, cards
    group_by cards, group_filters
  end

  # Gets only matching attributes of the +cards+ attributes and +filters+
  def clear_group_filters(filters, cards)
    return unless filters
    return unless cards

    card_attributes = []

    # Get all cards uniq attributes
    cards.each {|card| card_attributes = (card_attributes + card.keys).uniq}

    # Get only intersecting attributes
    card_attributes & filters.uniq
  end

  # Simple Recursive function that groups the given +cards+ array
  # by the given +filters+ array.
  # If there's more than one filter it nests the grouping inside each previous group
  def group_by(cards, filters = [], current_index = 0)
    current_filter = filters[current_index]
    return cards unless current_filter
    current_filter = current_filter.to_s

    # Group a first time
    res = cards.group_by {|g| g[current_filter]}

    # If any filters, just keep grouping with next filter
    res = res.map do |k, v|
      {k => group_by(v, filters, current_index + 1)}
    end if filters.any?

    res
  end

  def print_results
    App.print_cards self.cards
  end

  # Very Simple recursive function that prints the array of cards
  # Depending on the level, it shows several '#' to indicate depth
  def self.print_cards(cards, level = 1)
    return nil unless cards

    cards.each do |card, v|
      if card['name']
        puts "#{card['name']} - #{card['multiverseid']}"
      elsif v
        space = "#" * (level)
        puts "#{space} #{card} #{space}"
        print_cards v, level + 1
        puts "#{space} ------ #{space}"
      else
        print_cards card, level
      end
    end

  end

end
