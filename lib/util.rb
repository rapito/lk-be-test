require 'net/http'

class Util
  # A simple module to encapsulate cli flags and parameters to filter and group
  module FLAGS
    GROUPINGS = 'group'
    FILTERS = %w(name manaCost cmc colors colorIdentity type types subtypes rarity set setName text artist number power toughness layout multiverseid printings originalText originalType id)
  end

  # Module that handles HTTP requests
  module NET
    def self.request(url, params = {})
      uri = URI(url)
      params ||= {}
      uri.query = URI.encode_www_form(params)

      res = Net::HTTP.get_response(uri)
      res if res.is_a?(Net::HTTPSuccess)
    rescue StandardError => e
      # We should probably notify someone here
      nil
    end

    def self.get(url, params = {})
      return nil unless url
      request url, params
    end
  end

  # Obtains all filterable arguments declared on our filter flag on +FLAGS::FILTERS+
  def self.get_filter_args
    filters = {}
    Util::FLAGS::FILTERS.each do |filter|
      args = Util.get_args filter
      filters[filter] = args if args
    end

    filters
  end

  # Obtains arguments based on a flag name by fetching the next argument separated by commas
  # i.e ./cli flag_name flag_1
  # i.e ./cli flag_name flag_1,flag_2
  def self.get_args(flag_name)
    index = ARGV.index(flag_name) || ARGV.index(flag_name.chars.first)
    return nil unless index

    arg = ARGV[index + 1]
    return nil unless arg

    arg.split(',').uniq
  end

end