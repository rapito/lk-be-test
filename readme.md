# MTG CLI 
 
This application allows fetching of cards from 
[https:/api.magicthegathering.io/](https://magicthegathering.io/) and filtering the result via argument parameters. 

It does not use the API's filtering endpoints, instead it paginates the result on a multi-threaded context and filters/groups the results afterwards.  

## Usage

The cli allows the user to filter by any of the *allowed* parameters described in `Util::FLAGS::FILTERS` on `./lib/util.rb` file.

It also allows the user to group by any *attribute* that belongs to the cards.

* Main usage is as follows: 
    * Filtering: ```./cli.sh attribute_name comma_separated_values ```
    * Grouping: ```./cli.sh group comma_separated_attribute_names```

##### Examples

* ```./cli.sh group set``` 
    * Returns a list of **Cards** grouped by **`set`**.
* ```./cli.sh group set,rarity``` 
    * Returns a list of **Cards** grouped by **`set`** and then each **`set`** grouped by **`rarity`**.
* ```./cli.sh set KTK colors Red,Blue``` 
    * Returns a list of cards from the  **Khans of Tarkir (KTK)** that ONLY have the colours `red` AND `blue` (same results as https://api.magicthegathering.io/v1/cards?set=KTK&colors=Red,Blue)


## Code Features

* No 3rd Party libraries, only using Ruby's Libraries.
* Used Threading to parallelize Card fetching.
* Testing Included (No Batteries Required). Added a small testing file, check #Testing section

# Testing

A  small testing file containing a few tests is included, to execute simply run:

* ```./test.sh```